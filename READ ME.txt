Recommended web browser:
Firefox

Run the following commands on your command prompt for properly testing:
- pip install django-allauth
- pip install django-progressive-web-app
- pip install django-extensions Werkzeug pyOpenSSL
Run the following command to run the server:
- python manage.py runserver_plus --cert-file CERT_PATH

To log in visit:
https://localhost:8000/accounts/login

Add security exception
- For more info, search for .jpeg images on root directory (A�adirExp_1.jpeg, A�adirExp_2.jpeg)

Superuser: 
admin@elevator.com
123Admin

Testing Facebook Login
emails:
- eelnatqtyv_1548307445@tfbnw.net (already registered)
- nykzzefrsx_1548307448@tfbnw.net
- pgzjtxmabp_1548307447@tfbnw.net
passwords:
- 123User

Every step is highly important. 

Project running with a SSL Empty Certification, necessary for Allauth App.
Progressive Web App is running only on Firefox due to the SSL Cert. lack of security 
(But, same pwa settings and service workers were applied to the same project without Allauth app - Facebook Login and run just fine on Chrome)
