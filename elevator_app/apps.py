from django.apps import AppConfig


class ElevatorAppConfig(AppConfig):
    name = 'elevator_app'
