from django.contrib import admin
from .models import User, Client, Elevator, Odt, Detail

admin.site.register(User)
admin.site.register(Client)
admin.site.register(Elevator)
admin.site.register(Odt)
admin.site.register(Detail)
