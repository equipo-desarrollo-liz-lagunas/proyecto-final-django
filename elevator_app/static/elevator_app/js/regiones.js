function populate(s1, s2){
    var s1 = document.getElementById(s1);
    var s2 = document.getElementById(s2);
    s2.innerHTML = "";

    if(s1.value == "arica"){
        var optionArray = ["|", "arica|Arica"];
    }else if(s1.value == "tarapaca"){
        var optionArray = ["|", "iquique|Iquique", "altohospicio|Alto Hospicio"];
    }else if(s1.value == "antofagasta"){
        var optionArray = ["|", "antofagasta|Antofagasta", "calama|Calama", "tocopilla|Tocopilla"];
    }else if(s1.value == "atacama"){
        var optionArray = ["|", "copiapo|Copiapó", "vallenar|Vallenar"];
    }else if(s1.value == "coquimbo"){
        var optionArray = ["|", "coquimbo|Coquimbo", "laserena|La Serena"];
    }else if(s1.value == "valparaiso"){
        var optionArray = ["|", "valparaiso|Valparaíso", "viñadelmar|Viña del Mar"];
    }else if(s1.value == "santiago"){
        var optionArray = ["|", "santiago|Santiago"];
    }else if(s1.value == "ohiggins"){
        var optionArray = ["|", "rancagua|Rancagua", "sanfernando|San Fernando"];
    }else if(s1.value == "maule"){
        var optionArray = ["|", "talca|Talca", "curico|Curicó", "linares|Linares"];
    }else if(s1.value == "biobio"){
        var optionArray = ["|", "concepcion|Concepción", "losangeles|Los Ángeles"];
    }else if(s1.value == "ñuble"){
        var optionArray = ["|", "chillan|Chillán", "sancarlos|San Carlos"];
    }else if(s1.value == "araucania"){
        var optionArray = ["|", "temuco|Temuco", "villarica|Villa Rica", "pucon|Pucón"];
    }else if(s1.value == "rios"){
        var optionArray = ["|", "valdivia|Valdivia", "panguipulli|Panguipulli"];
    }else if(s1.value == "lagos"){
        var optionArray = ["|", "puertomontt|Puerto Montt", "puertovaras|Puerto Varas", "osorno|Osorno", "castro|Castro"];
    }else if(s1.value == "aysen"){
        var optionArray = ["|", "coyhaique|Coyhaique", "puertoaysen|Puerto Aysen"];
    }else if(s1.value == "antartica"){
        var optionArray = ["|", "puntaarenas|Punta Arenas", "puertonatales|Puerto Natales"];
    }

    for (var option in optionArray) {
        var pair = optionArray[option].split("|");
        var newOption = document.createElement("option");
        newOption.value = pair[0];
        newOption.innerHTML = pair[1];
        s2.options.add(newOption); 
    }
}

function checkRut(rut) {

    var valor = rut.value.replace('.','');
    valor = valor.replace('-','');

    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    
    rut.value = cuerpo + '-'+ dv
    
    if(cuerpo.length < 7) { 
        alert("Rut incompleto");
        rut.setCustomValidity("RUT Incompleto"); 
        return false;}
    
    suma = 0;
    multiplo = 2;
    
    for(i=1;i<=cuerpo.length;i++) {
    
        index = multiplo * valor.charAt(cuerpo.length - i);
        
        suma = suma + index;
        
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
  
    }
    
    dvEsperado = 11 - (suma % 11);
    
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;
    

    if(dvEsperado != dv) { 
        alert("Rut inválido")
        rut.setCustomValidity("RUT Inválido"); 
        return false; 
    }else{
        alert("Rut validado con éxito");
    }
    

    rut.setCustomValidity('');
}

function validateYear(fecha1){
    var input = document.getElementById('fecha1').value;
    var fecha = new Date(input);

    if ( !!d.valueOf() ){
    var year = fecha.getFullYear();
        if(year<2001){
            alert("Fecha validad con éxito");
        }else{
            alert("Usted debe tener año de nacimiento menor a 2001");
        }
    }else{
        alert("Fecha inválida");
    }
}

function phonenumber(inputtxt)
{
  var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{5})$/;
  if((inputtxt.value.match(phoneno)))
        {
      return true;
        }
      else
        {
        alert("Ingresa un número válido. El formato es: +XXX12345678");
        return false;
        }
}

function submitClickTech() {
    if (techValidation()) {
      alert("Gracias por tu tiempo! Los datos han sido registrados :)");
      return true;
    } else {
      return false;
    }
  }

  function techValidation() {
    flag = true;

    if (document.techForm.name.value == "") {
      alert("Por favor, completa el nombre!");
      flag = false;
    }

    if (document.techForm.phone.value == "") {
      alert("Por favor completa tu número de teléfono!");
      flag = false;
    }

    if (document.techForm.password1.value == "") {
        alert("Debes ingresar la contraseña");
        flag = false;
    }

    if (document.techForm.password2.value == "") {
        alert("Debes ingresar la contraseña");
        flag = false;
    }

    return flag;
  }

  function submitClickClient() {
    if (clientValidation()) {
      alert("Gracias por tu tiempo, el cliente ha sido registrado :) !");
      return true;
    } else {
      return false;
    }
  }

  function clientValidation() {
    flag = true;

    if (document.clientForm.name.value == "") {
        alert("Por favor, completa el nombre!");
        flag = false;
    }

    if (document.clientForm.address.value == "") {
        alert("Por favor, completa la dirección!");
        flag = false;
      }

    if (document.clientForm.phone.value == "") {
        alert("Por favor completa tu número de teléfono!");
        flag = false;
    }

    if (document.clientForm.email.value == "") {
        alert("Por favor ingresa el correo electrónico!");
        flag = false;
    }

    if (document.clientForm.region.selectedIndex == 0) {
      alert("Por favor selecciona una región de la lista!");
      flag = false;
    }

    if (document.clientForm.city.selectedIndex == 0) {
        alert("Por favor selecciona una ciudad de la lista!");
        flag = false;
      }

    return flag;
  }

  function submitClickDetail() {
    if (detailValidation()) {
      alert("Gracias por tu tiempo, el detalle ha sido registrado :) !");
      return true;
    } else {
      return false;
    }
  }

  function detailValidation() {
    flag = true;

    if (document.detailForm.detail.value == "") {
        alert("Por favor, completa la descripción!");
        flag = false;
    }

    if (document.detailForm.failure.value == "") {
        alert("Por favor, completa las fallas. En caso de no haber, regístralo por escrito.");
        flag = false;
      }

    if (document.detailForm.elevator.selectedIndex == 0) {
        alert("Por favor selecciona un ascensor de la lista!");
        flag = false;
      }

    return flag;
  }

  