from __future__ import unicode_literals
from django.db import models
from datetime import date
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from .managers import UserManager

class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True, error_messages={'unique':"This email has already been registered"})
    name = models.CharField(_('name'), max_length=100, blank=True)
    phone = models.CharField(_('phone number'), max_length=12, blank=True)
    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('staff'), default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_name(self):
        '''
        Returns name for the user.
        '''
        return self.name

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def _str_(self):
        return self.email

    def _unicode_(self):
        return self.email

    def has_perm(self, perm, obj=None):
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        # Simplest possible answer: Yes, always
        return True

class Client(models.Model):
    name = models.CharField(_('name'), max_length=100, blank=True)
    address = models.CharField(_('address'), max_length=150, blank=True)
    region = models.CharField(_('region'), max_length=30, blank=True)
    city = models.CharField(_('city'), max_length=30, blank=True)
    phone = models.CharField(_('phone number'), max_length=12, blank=True)
    email = models.EmailField(_('email address'), unique=True)
    tech = models.ForeignKey('User', on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = _('client')
        verbose_name_plural = _('clients')

    def get_name(self):
        '''
        Returns name for the user.
        '''
        return self.name

class Elevator(models.Model):
    model = models.CharField(_('model'), max_length=100, blank=True)

    class Meta:
        verbose_name = _('elevator')
        verbose_name_plural = _('elevators')

    def get_model(self):
        '''
        Returns name for the user.
        '''
        return self.model

class Odt(models.Model):
    tech = models.ForeignKey('User', on_delete=models.CASCADE)
    client = models.ForeignKey('Client', on_delete=models.CASCADE)
    start_date = models.DateField(_("Start Date"))
    start_time = models.TimeField(_("Start Time"))
    end_date = models.DateField(_("End Date"), null=True)
    end_time = models.TimeField(_("End Time"), null=True)

    class Meta:
        verbose_name = _('odt')
        verbose_name_plural = _('odts')


class Detail(models.Model):
    elevator = models.ForeignKey('Elevator', on_delete=models.CASCADE)
    odt = models.ForeignKey('Odt', on_delete=models.CASCADE)
    detail = models.TextField()
    failure = models.TextField(null=True, blank=True)
    piezas = models.TextField(null=True, blank=True)